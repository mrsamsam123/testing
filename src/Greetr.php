<?php

    namespace Samplify\Greetr;

    class Greetr{
        public function greet(String $name){
            return "Hi $name ! How are you today!";
        }
    }
